const departements = [{"code":"01","nom":"Ain"},{"code":"02","nom":"Aisne"},{"code":"03","nom":"Allier"},{"code":"04","nom":"Alpes-de-Haute-Provence"},{"code":"05","nom":"Hautes-Alpes"},{"code":"06","nom":"Alpes-Maritimes"},{"code":"07","nom":"Ardèche"},{"code":"08","nom":"Ardennes"},{"code":"09","nom":"Ariège"},{"code":"10","nom":"Aube"},{"code":"11","nom":"Aude"},{"code":"12","nom":"Aveyron"},{"code":"13","nom":"Bouches-du-Rhône"},{"code":"14","nom":"Calvados"},{"code":"15","nom":"Cantal"},{"code":"16","nom":"Charente"},{"code":"17","nom":"Charente-Maritime"},{"code":"18","nom":"Cher"},{"code":"19","nom":"Corrèze"},{"code":"21","nom":"Côte-d’or"},{"code":"22","nom":"Côtes-d’armor"},{"code":"23","nom":"Creuse"},{"code":"24","nom":"Dordogne"},{"code":"25","nom":"Doubs"},{"code":"26","nom":"Drôme"},{"code":"27","nom":"Eure"},{"code":"28","nom":"Eure-et-Loir"},{"code":"29","nom":"Finistère"},{"code":"2A","nom":"Corse-du-Sud"},{"code":"2B","nom":"Haute-Corse"},{"code":"30","nom":"Gard"},{"code":"31","nom":"Haute-Garonne"},{"code":"32","nom":"Gers"},{"code":"33","nom":"Gironde"},{"code":"34","nom":"Hérault"},{"code":"35","nom":"Ille-et-Vilaine"},{"code":"36","nom":"Indre"},{"code":"37","nom":"Indre-et-Loire"},{"code":"38","nom":"Isère"},{"code":"39","nom":"Jura"},{"code":"40","nom":"Landes"},{"code":"41","nom":"Loir-et-Cher"},{"code":"42","nom":"Loire"},{"code":"43","nom":"Haute-Loire"},{"code":"44","nom":"Loire-Atlantique"},{"code":"45","nom":"Loiret"},{"code":"46","nom":"Lot"},{"code":"47","nom":"Lot-et-Garonne"},{"code":"48","nom":"Lozère"},{"code":"49","nom":"Maine-et-Loire"},{"code":"50","nom":"Manche"},{"code":"51","nom":"Marne"},{"code":"52","nom":"Haute-Marne"},{"code":"53","nom":"Mayenne"},{"code":"54","nom":"Meurthe-et-Moselle"},{"code":"55","nom":"Meuse"},{"code":"56","nom":"Morbihan"},{"code":"57","nom":"Moselle"},{"code":"58","nom":"Nièvre"},{"code":"59","nom":"Nord"},{"code":"60","nom":"Oise"},{"code":"61","nom":"Orne"},{"code":"62","nom":"Pas-de-Calais"},{"code":"63","nom":"Puy-de-Dôme"},{"code":"64","nom":"Pyrénées-Atlantiques"},{"code":"65","nom":"Hautes-Pyrénées"},{"code":"66","nom":"Pyrénées-Orientales"},{"code":"67","nom":"Bas-Rhin"},{"code":"68","nom":"Haut-Rhin"},{"code":"69","nom":"Rhône"},{"code":"70","nom":"Haute-Saône"},{"code":"71","nom":"Saône-et-Loire"},{"code":"72","nom":"Sarthe"},{"code":"73","nom":"Savoie"},{"code":"74","nom":"Haute-Savoie"},{"code":"75","nom":"Paris"},{"code":"76","nom":"Seine-Maritime"},{"code":"77","nom":"Seine-et-Marne"},{"code":"78","nom":"Yvelines"},{"code":"79","nom":"Deux-Sèvres"},{"code":"80","nom":"Somme"},{"code":"81","nom":"Tarn"},{"code":"82","nom":"Tarn-et-Garonne"},{"code":"83","nom":"Var"},{"code":"84","nom":"Vaucluse"},{"code":"85","nom":"Vendée"},{"code":"86","nom":"Vienne"},{"code":"87","nom":"Haute-Vienne"},{"code":"88","nom":"Vosges"},{"code":"89","nom":"Yonne"},{"code":"90","nom":"Territoire-de-Belfort"},{"code":"91","nom":"Essonne"},{"code":"92","nom":"Hauts-de-Seine"},{"code":"93","nom":"Seine-Saint-Denis"},{"code":"94","nom":"Val-de-Marne"},{"code":"95","nom":"Val-d’oise"}],
getPostcodeByName = name => {
  return departements.filter( dep => {
    return dep.nom == name
  })
},
createCircle = (lat,lng) => {
  let turfCircle = turf.circle([lng, lat], 100),
  shapeArray = [],

  // on sait que notre cercle turf est un polygon
  // on sait que la metropole est un multipolygon
  // on parcours les elements en les transformant en polygon
  // et intersect
  metropoleElements = metropole.geometry.coordinates;

  metropoleElements.forEach( (e,index) => {
    let feat={'type':'Polygon','coordinates':metropoleElements[index]},
    tmpCircle = turf.intersect(turfCircle, feat);
    shapeArray.push(L.geoJSON(tmpCircle))
  })

  return L.layerGroup(shapeArray)
},
createMarker = (lat,lng,city) => {
  let marker = L.marker([lat, lng]),
  precision = 10000
  marker.bindPopup(`<b>${city}</b><br>[ ${Math.round(lat*precision)/precision}, ${Math.round(lng*precision)/precision} ]`).openPopup();
  return marker
},
createPoint = (lat,lng,city,departement=null) => {
  let marker = createMarker(lat,lng,city),
  circle = createCircle(lat,lng),
  layer = null

  if(departement != null) {
    fetch('geojson/'+departement+'.geojson')
    .then(response => response.json())
    .then(datas => {
      layer = L.layerGroup([marker,circle, L.geoJSON(datas) ])
      layersArray.push(layer)
      addToLayerGroup(layer)
    })
  }
  else {
    layer = L.layerGroup([marker,circle])
    layersArray.push(layer)
    addToLayerGroup(layer)
  }
},
addToLayerGroup = (elm) => {
  layerGroup.addLayer(elm)
},
removeLastPoint = _ => {
  if(layersArray.length > 0) {
    let layer = layersArray.pop()
    layerGroup.removeLayer(layer)
  }
},
clearLayer = _ => {
  layerGroup.clearLayers()
}

let theMap = L.map('map').setView([46.415,1.461], 6),
metropole = null,
reverseURL = 'https://nominatim.openstreetmap.org/reverse',
layersArray = [],
layerGroup = L.layerGroup()
layerGroup.addTo(theMap)

fetch('geojson/metropole.geojson')
.then(response => response.json())
.then(datas => {
  metropole = datas
})

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
  attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap contributors</a>',
  maxZoom: 19,
}).addTo(theMap)

L.Control.geocoder({
  defaultMarkGeocode: false,
  geocoder: new L.Control.Geocoder.Nominatim({
    geocodingQueryParams: {
      'accept-language': 'fr',
      countrycodes: 'fr'
    },
    reverseQueryParams: {
      'accept-language': 'fr'
    }
  })
})
.on('markgeocode', (e) => {
  let center = e.geocode.center,
  postCode = e.geocode.properties.address.postcode,
  departementRef = null,
  city = e.geocode.properties.address.city != undefined ? e.geocode.properties.address.city : e.geocode.properties.address.village

  if(postCode != null) {
    departementRef = postCode.substring(0,2).toUpperCase()
  }
  else if(e.geocode.properties.address.county != null) {
    if(getPostcodeByName(e.geocode.properties.address.county).length > 0) {
      departementRef = getPostcodeByName(e.geocode.properties.address.county)[0].code
    }
    else {
      departementRef = null
    }
  }

  createPoint(center.lat,center.lng,city,departementRef)
})
.addTo(theMap)

theMap.on('click', (e) => {
  let lat = e.latlng.lat,
  lon = e.latlng.lng,
  city = '',
  params = `?lat=${lat}&lon=${lon}&accept-language=fr&format=json&zoom=18`

  fetch(reverseURL+params)
  .then(response => response.json())
  .then(datas => {
    // https://nominatim.org/release-docs/develop/api/Reverse/
    // le "county" ne donne pas toujours le département mais on a le postcode qui lui est toujours juste CQFD
    // comme à Marseille (inexistant) ou Lyon (Métropole de lyon au lieu de rhone)

    if(datas.address != undefined && datas.address.country == 'France') {
      let departementRef = datas.address.postcode.substring(0,2)
      // cas particulier la corse (postcode en 20 au lieu de 2A et 2B)
      // on doit coupler la recherche avec le county
      // je suis presque pas surpris ...
      if(departementRef == 20 ) {
        departementRef = (datas.address.county == 'Corse-du-Sud') ? '2A' : '2B'
      }

      city = datas.address.city != undefined ? datas.address.city : datas.address.village
      createPoint(lat,lon,city,departementRef)
    }
  })
})

const customControlClean =  L.Control.extend({
  options: {
    position: 'topleft'
  },

  onAdd: function () {
    let clearButton = L.DomUtil.create('button')
    clearButton.innerHTML = "Réinitialiser la carte"
    clearButton.onclick = ev => {
      L.DomEvent.stopPropagation(ev);
      clearLayer()
    }

    return clearButton;
  }
}),
customControlPoint = L.Control.extend({
  options: {
    position: 'topleft'
  },

  onAdd: function () {
    let removeLastLayer = L.DomUtil.create('button')
    removeLastLayer.innerHTML = "Défaire le dernier point"
    removeLastLayer.onclick = ev => {
      L.DomEvent.stopPropagation(ev);
      removeLastPoint()
    }

    return removeLastLayer;
  }
}),
customControlLocateMe = L.Control.extend({
  options: {
    position: 'topright'
  },

  onAdd: function () {
    let locateMe = L.DomUtil.create('button')
    locateMe.innerHTML = "Localise moi"
    locateMe.onclick = ev => {
      L.DomEvent.stopPropagation(ev);

      navigator.geolocation.getCurrentPosition(position => {
        let params = `?lat=${position.coords.latitude}&lon=${position.coords.longitude}&format=json&zoom=18`

        fetch(reverseURL+params)
        .then(response => response.json())
        .then(datas => {
          if(datas.address != undefined && datas.address.country == 'France') {
            let departementRef = datas.address.postcode.substring(0,2),
            city = datas.address.city != undefined ? datas.address.city : datas.address.village
            createPoint(position.coords.latitude,position.coords.longitude,city,departementRef)
          }
        })
      })
    }
    return locateMe;
  }
})

theMap.addControl(new customControlPoint());
theMap.addControl(new customControlClean());

// navigator.geolocation needs HTTPS to works
if ( location.protocol == 'https:' ) {
  theMap.addControl(new customControlLocateMe());
}
